#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, RCTBridgeDelegate>

@property (nonatomic, strong) UIWindow *window;

+(void)openCCTV:(NSString *)accessToken appKey:(NSString *)appKey serialNumber:(NSString *)serialNumber verificationCode:(NSString *)verificationCode apiServerURL:(NSString *)apiServerURL authServer:(NSString *)authServer cameraName:(NSString *)cameraName callback:(void(^)(void))callback;

@end
