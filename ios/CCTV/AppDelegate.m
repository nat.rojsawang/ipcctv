#import "AppDelegate.h"

#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

#ifdef FB_SONARKIT_ENABLED
#import <FlipperKit/FlipperClient.h>
#import <FlipperKitLayoutPlugin/FlipperKitLayoutPlugin.h>
#import <FlipperKitUserDefaultsPlugin/FKUserDefaultsPlugin.h>
#import <FlipperKitNetworkPlugin/FlipperKitNetworkPlugin.h>
#import <SKIOSNetworkPlugin/SKIOSNetworkAdapter.h>
#import <FlipperKitReactPlugin/FlipperKitReactPlugin.h>

static void InitializeFlipper(UIApplication *application) {
  FlipperClient *client = [FlipperClient sharedClient];
  SKDescriptorMapper *layoutDescriptorMapper = [[SKDescriptorMapper alloc] initWithDefaults];
  [client addPlugin:[[FlipperKitLayoutPlugin alloc] initWithRootNode:application withDescriptorMapper:layoutDescriptorMapper]];
  [client addPlugin:[[FKUserDefaultsPlugin alloc] initWithSuiteName:nil]];
  [client addPlugin:[FlipperKitReactPlugin new]];
  [client addPlugin:[[FlipperKitNetworkPlugin alloc] initWithNetworkAdapter:[SKIOSNetworkAdapter new]]];
  [client start];
}
#endif

static UIWindow *_window = nil;
static UIWindow *reactNativeWindow = nil;
static UIWindow *iotWindow = nil;

@implementation AppDelegate

+ (void)openCCTV:(NSString *)accessToken appKey:(NSString *)appKey serialNumber:(NSString *)serialNumber verificationCode:(NSString *)verificationCode apiServerURL:(NSString *)apiServerURL authServer:(NSString *)authServer cameraName:(NSString *)cameraName callback:(void(^)(void))callback {
  // iotWindow = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
  // UIViewController* rootVC = [[UIViewController alloc] init];
  
  // iotWindow.backgroundColor = [UIColor clearColor];
  // iotWindow.rootViewController = rootVC;
      
  // IOTViewController* vc = [[IOTViewController alloc] init];
  // vc.modalPresentationStyle = UIModalPresentationFullScreen;
  // vc.accessToken = accessToken;
  // vc.appKey = appKey;
  // vc.serialNumber = serialNumber;
  // vc.verificationCode = verificationCode;
  // vc.apiServerURL = apiServerURL;
  // vc.authServer = authServer;
  // vc.cameraName = cameraName;
  // vc.closeCallback = ^() {
  //   _window = reactNativeWindow;
  //   [_window makeKeyAndVisible];
  // };

  // vc.failCallback = ^() {
  //   _window = reactNativeWindow;
  //   [_window makeKeyAndVisible];
    
  //   callback();
  // };
  
  // _window = iotWindow;
  // [_window makeKeyAndVisible];

  // [rootVC presentViewController:vc animated:YES completion:nil];

  callback();
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  #ifdef FB_SONARKIT_ENABLED
    InitializeFlipper(application);
  #endif

    RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
    RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                    moduleName:@"CCTV"
                                              initialProperties:nil];

    rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UIViewController *rootViewController = [UIViewController new];
    rootViewController.view = rootView;
    self.window.rootViewController = rootViewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
  #if DEBUG
    return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  #else
    return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
  #endif
}

@end
