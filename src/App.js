import React, {useState} from 'react';

import {StyleSheet, Button, View, NativeModules} from 'react-native';
// import IPCamera from 'react-native-ip-camera';
import CCTVPOC from 'react-native-react-native-cctvpoc';

const {AccessibilityManager, IpCamera} = NativeModules;
const serialNumber = '779952598';
const verificationCode = 'OCMRPH';
const accessToken =
  'at.76wanrsr3c9mihcob9pst1g4bha23h36-2kqdkuh1e7-163oogd-3ni2ce91d';
const appKey = 'e34d2f451c7043a98661e058ecf00369';
const apiServerURL = '';
const authServer = '';
const cameraName = '';
export default function App() {
  const [isStartFail, setIsStartFail] = useState(false);
  console.log('APP', {
    NativeModules,
    IpCamera,
    CCTVPOC,
  });

  const onPress = () => {
    IpCamera.createCalendarEvent(
      'testName',
      'testLocation',
      (eventId) => {
        console.log(`Created a new event with id ${eventId}`);
      },
      // (error) => {
      //   console.error(`Error found! ${error}`);
      // },
      // (eventId) => {
      //   console.log(`event id ${eventId} returned`);
      // },
    );
  };

  return (
    <View style={styles.container}>
      <Button
        onPress={onPress}
        // onPress={() =>
        //   IPCamera.openIPCamera(
        //     serialNumber,
        //     verificationCode,
        //     accessToken,
        //     appKey,
        //     apiServerURL,
        //     authServer,
        //     cameraName,
        //     () => {
        //       setIsStartFail(true);
        //     },
        //   )
        // }
        title="OPEN IP Camera"
        color="#841584"
      />
      <Button
        onPress={() => {
          CCTVPOC.openCCTVPOC(
            accessToken,
            appKey,
            serialNumber,
            verificationCode,
            apiServerURL,
            authServer,
            cameraName,
          );
        }}
        title="Click Open CCTV"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
